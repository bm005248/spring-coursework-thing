
#ifndef ROOM_H
#define ROOM_H

#include <string>
#include <map>

class Room {
private:
    std::string description;
    std::map<std::string, Room*> exits;

public:
    Room(const std::string& description);
    void AddExit(const std::string& direction, Room* room);
    Room* GetExit(const std::string& direction);
};

#endif 