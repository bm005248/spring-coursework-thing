#include <iostream>
#include "Area.h"
#include "Character.h"
#include "CommandInterpreter.h"
#include "Player.h"
#include "Room.h"
#include "Item.h"

int main() {
    Player player("Hero", 100); 
    Area area;
    CommandInterpreter interpreter(&player);

    std::string command;
    while (true) {
        std::cout << "> ";
        std::getline(std::cin, command);

        if (command == "quit") {
            break;
        }

        if (playerChoosesToUseTimeMachine) {
            player.useTimeMachine();

        interpreter.interpretCommand(command);
    }

    return 0;
}

