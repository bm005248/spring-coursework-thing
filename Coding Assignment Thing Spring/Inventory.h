#ifndef INVENTORY_H
#define INVENTORY_H

#include <vector>
#include "GameObject.h"
#include "Character.h"
#include "Character.cpp"

class Inventory {
private:
    std::vector<GameObject*> items;

public:
    void addItem(GameObject* item);
    void useItem(GameObject* item, GameObject& target);
};

#endif 
