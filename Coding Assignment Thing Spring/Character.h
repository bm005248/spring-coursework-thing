
#ifndef CHARACTER_H
#define CHARACTER_H

#include <string>
#include <vector>
#include "Item.h"
#include "Inventory.h"

class Character {
protected:
    std::string name;
    int health;
    std::vector<Item*> inventory;

public:
    Character(const std::string& name, int health);
    void TakeDamage(int damage);
    void AddItem(const Item& item);
    void UseItem(const Item& item);
    virtual ~Character();

    void AddItem(Item* item);
    inventory;.addItem(item); 

    Character() = default;
}

#endif 
