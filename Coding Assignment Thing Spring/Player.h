
#ifndef PLAYER_H
#define PLAYER_H

#include "Character.h"

class Player : public Character {
public:
    Player(const std::string& name, int health);
    void Move(const std::string& direction);
    void Look() const;
    void PickUpItem(const std::string& item);
    void useTimeMachine();
};

#endif 
