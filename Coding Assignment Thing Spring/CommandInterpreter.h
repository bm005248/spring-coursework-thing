
#ifndef COMMANDINTERPRETER_H
#define COMMANDINTERPRETER_H

#include <string>

class Player; 

class CommandInterpreter {
private:
    Player* player;
public:
    CommandInterpreter(Player* player);
    void interpretCommand(const std::string& command);
};

#endif 

