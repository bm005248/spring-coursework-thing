#ifndef TIMEMACHINE_H
#define TIMEMACHINE_H

#include <vector>
#include "Era.h"

class TimeMachine {
public:
    TimeMachine();
    void travelToEra(const std::string& eraName);
    Era* getCurrentEra();

private:
    std::vector<Era*> eras;
    Era* currentEra;
};

#endif 