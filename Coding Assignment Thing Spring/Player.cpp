#include "Player.h"

Player::Player(const std::string& name, int health) : Character(name, health) {}

void Player::Move(const std::string& direction) {
}

void Player::Look() const {
}

void Player::PickUpItem(const std::string& item) {
}

void Player::useTimeMachine() {
}

void Player::performAction() {

    if (playerChoosesToUseTimeMachine) {
        this->useTimeMachine();
    }
}