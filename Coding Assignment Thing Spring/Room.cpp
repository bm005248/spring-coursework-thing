
#include "Room.h"

Room::Room(const std::string& description) : description(description) {}

void Room::AddExit(const std::string& direction, Room* room) {
    exits[direction] = room;
}

Room* Room::GetExit(const std::string& direction) {
    auto it = exits.find(direction);
    if (it != exits.end()) return it->second;
    return nullptr;
}


