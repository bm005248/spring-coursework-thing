#ifndef ERA_H
#define ERA_H

#include <string>
#include <vector>
#include "Room.h" 

class Era {
public:
    Era(const std::string& name, const std::string& description);
    void enterEra();

private:
    std::string name;
    std::string description;
    std::vector<Room*> rooms; 
};

#endif 
